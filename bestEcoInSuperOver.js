const matchData = require('./readCsv');

function bestEcoInSuperOver(data){
    let superOverRun = {};

    for(let index of data){
        if(data[index["is_super_over"]] && data[index["bowling_team"]]){
            superOverRun[index["bowler"]] += (superOverRun[index["is_super_over"]])
        }
        else{
            if(data[index["is_super_over"]] && !data[index["bowling_team"]]){
                superOverRun[index["bowler"]] = (superOverRun[index["is_super_over"]]);
            }
           
        }
    }
    return superOverRun;
}

let answer = bestEcoInSuperOver(matchData.deliveries);

console.log(answer);