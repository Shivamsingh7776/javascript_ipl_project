let csvToJson = require('convert-csv-to-json');

matches = csvToJson.fieldDelimiter(',').getJsonFromCsv('src/server/data/matches.csv');
deliveries = csvToJson.fieldDelimiter(',').getJsonFromCsv('src/server/data/deliveries.csv');



module.exports = {
    deliveries: deliveries,
    matches: matches
};