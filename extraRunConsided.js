const matchData = require('./readCsv');

function extraRunConsided(deliveriesData, matchesData) {
    let matchId2016 = [];
    for (let index1 of matchesData) {
        // console.log(index1);
        if (index1["season"] == "2016") {
            matchId2016.push(index1["id"]);

        }
    }
    let extraRunConsidedPerTeam = {};
    
    for (let delivery of deliveriesData) {
        if (matchId2016.includes(delivery["match_id"])) {
            if (extraRunConsidedPerTeam[delivery["bowling_team"]]) {
                extraRunConsidedPerTeam[delivery["bowling_team"]] += Number(delivery["extra_runs"]);
               
            } else {
                extraRunConsidedPerTeam[delivery["bowling_team"]] = Number(delivery["extra_runs"]);
            }
        }
    }
    return extraRunConsidedPerTeam;



}

let answer = extraRunConsided(matchData.deliveries, matchData.matches);

console.log(answer);