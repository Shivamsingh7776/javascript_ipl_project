const matchData = require('./readCsv');

function wonTheTossWonTheMatch(data) {

    let winnerTeam = {};

    for (let index of data) {
        if (index["toss_winner"] == index["winner"]) {
            if(!winnerTeam[index["winner"]])
            {
                winnerTeam[index["winner"]] = 1;
            }
            else{
                winnerTeam[index["winner"]] += 1;
            }
        }
        
        
    }
    return winnerTeam;
}

let answer = wonTheTossWonTheMatch(matchData.matches);
console.log(answer);