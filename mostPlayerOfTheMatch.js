const matchData = require('./readCsv');

function mostPlayerOfTheMatch(data) {
    let mostManOfTheMatch = {};

    for (let index of data) {
        if (mostManOfTheMatch[index["season"]] && mostManOfTheMatch[index["season"]][index["player_of_match"]]) {
            mostManOfTheMatch[index["season"]][index["player_of_match"]] += 1;
        }
        else if (!mostManOfTheMatch[index["season"]]) {
            mostManOfTheMatch[index["season"]] = {};
            mostManOfTheMatch[index["season"]][index["player_of_match"]] = 1;
        }
        else {
            mostManOfTheMatch[index["season"]][index["player_of_match"]] = 1;
        }

        
    }
    
    return mostManOfTheMatch;
}

let answer = mostPlayerOfTheMatch(matchData.matches);

console.log(answer);