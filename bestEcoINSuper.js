const matchData = require('./readCsv');

function bestEcoInSuperOver(data) {
    let total_run = {};
    let total_ball = {};
    let superOverRun = {};
    for (let index of data) {
        if (index["is_super_over"] != 0) {
            if (total_run[index["bowler"]]) {
                total_run[index["bowler"]] += Number(index["total_runs"]);
            } else {
                total_run[index["bowler"]] = Number(index["total_runs"]);
            }
        }
        else {
            if (index["is_super_over"] != 0) {
                total_run[index["bowler"]] = Number(index["total_runs"]);
            }
        }
    }
    // console.log(total_run);
    for (let index of data) {
        if (index["is_super_over"] != 0) {
            if (total_ball[index["bowler"]]) {
                total_ball[index["bowler"]] += 1;
            }
            else {

                total_ball[index["bowler"]] = 1
            }
        } else {
            if (index["is_super_over"] != 0) {
                total_ball[index["bowler"]] = 1;
            }
        }
    }
    for(let index in total_run){
        superOverRun[index] = (total_run[index]/total_ball[index]) * 6;
    }
    const topEco = Object.fromEntries(
        Object.entries(superOverRun).sort(([, a], [, b]) => a - b)
    );

    let topTenEco = {};
    count = 0;
    for (let index in topEco) {
        if (count < 1) {
            topTenEco[index] = topEco[index];
            count += 1;

        } else {
            break;
        }



    }

    return topTenEco;
}

let answer = bestEcoInSuperOver(matchData.deliveries);
console.log(answer);

