const matchData = require('./readCsv');

function topEconomiacalBowler(deliviesData, matchData) {
    let data2015 = [];
    let totalRuns = [];
    for (let index1 of matchData) {
        if (index1["season"] === "2015") {
            data2015.push(index1["id"]);
        }
    }
    let topEconomicalBowler2015 = {};
    let totalRun = {};
    let totalBall = {};


    for (let index of deliviesData) {

        if (data2015.includes(index["match_id"])) {
            if (totalRun[index["bowler"]]) {
                totalRun[index["bowler"]] += Number(index["total_runs"]);
            } else {

                totalRun[index["bowler"]] = Number(index["total_runs"]);
            }

        }
    }

    for (let index of deliviesData) {
        if (data2015.includes(index["match_id"])) {
            if (totalBall[index["bowler"]]) {
                totalBall[index["bowler"]] += 1;
            } else {
                totalBall[index["bowler"]] = 1;
            }
        }
    }


    for (let index in totalRun) {
        topEconomicalBowler2015[index] = ((totalRun[index] / (totalBall[index])) * 6).toFixed(2);

    }


    const sortable = Object.fromEntries(
        Object.entries(topEconomicalBowler2015).sort(([, a], [, b]) => a - b)
    );
    let topTenEco = {};
    count = 0;
    for (let index in sortable) {
        if (count < 10) {
            topTenEco[index] = sortable[index];
            count += 1;

        } else {
            break;
        }



    }

    return topTenEco;


}


let answer = topEconomiacalBowler(matchData.deliveries, matchData.matches);

console.log(answer);