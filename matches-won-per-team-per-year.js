const matchData = require('./readCsv');

function matchesPerTeamPerYear(data) {
    let matchesPerTeamPerSeason = {};

    for (let index of data) {
        if ([index["season"]]) {
            if (matchesPerTeamPerSeason[index["season"][index["winner"]]]) {
                matchesPerTeamPerSeason[index["season"][index["winner"]]] += 1;

            }else {
                matchesPerTeamPerSeason["season"] = {};
                if(!matchesPerTeamPerSeason[index["season"][index["winner"]]]){
                    matchesPerTeamPerSeason[index["season"][index["winner"]]] = 1;
                }
                
            }
        }
        
    }
    return matchesPerTeamPerSeason;
}

let answer = matchesPerTeamPerYear(matchData.matches);
console.log(answer);