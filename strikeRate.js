const matchData = require('./readCsv');

function strikeRate(data) {
    let total_run = {};
    let ball_face = {};
    for (let index of data) {
        if (total_run[index["batsman"]]) {
            // total_run[index["batsman"]] += Number(index["batsman_runs"]);
            ball_face[index["batsman"]] += 1;
        } else {
            // total_run[index["batsman"]] = Number(index["batsman_runs"]);
            ball_face[index["batsman"]] = 1;
        }
    }
   
   
    return ball_face;

}


let answer = strikeRate(matchData.deliveries);

console.log(answer);